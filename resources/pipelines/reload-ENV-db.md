## Description

Reload databases from a duplicity backup or another environment.

It works for several databases, and optionally can run sanitization tasks or generate artifacts.

## Requirements

* Duplicity and mysql-client installed on the Jenkins node
* Access to the backups from the Jenkins node
* Access to the database server from the Jenkins node

## Example pipeline

```
_databases:
  bde:
    db_name: "bde_{{ appenv }}"
  bdm:
    db_name: "bdm_{{ appenv }}"
  nru:
    db_name: "nru_{{ appenv }}"

_envs:
  devel:
    target: 'dev'
    branch: 'master'
  testing:
    target: 'dev'
    branch: 'testing'
  staging:
    target: 'dev'
    branch: 'production'
  pro:
    target: 'wh'
    branch: 'production'
    trigger: false

pipelines:
  reload-ENV-db:
    config:
      ansible:
        extra_files:
          - 'ansible/tasks/db-sanitize-drush.yml'
    vars:
      databases: '#!groovy config._databases'
      environments: '#!groovy config._envs'
      duplicity_pass: '<secret>'
      duplicity_source_url: 'rsync://ext_back@backuphost.dev//data/backups'
      sanitize_tasks: "{{ workspace }}/ansible/tasks/db-sanitize-drush.yml"
      db_host: 'dbhost.dev'
      # job parameters
      environment: 'staging'
      source: 'From backup'
      environment_src: '-NOT APPLICABLE-'
      artifact: true
      sanitize: true
```

## Implementation

The pipeline provides this steps:

1. `extract-backup`: Extract source database from backup
2. `dump-source`: Dump database from a source environment
3. `reload`: Reload database on a target environment, sanitize and generate artifact


Steps 2 and 3 are performed with Ansible, and uses the same playbook ([reload-db.yml](/resources/ansible/playbooks/reload-db.yml)).

The reload playbook performs dump or reload tasks in parallel with a given concurrency.
