#!groovy
@Library('keynes') _

/* Job vars */
def LOCK = env.JOB_NAME
Integer TIMEOUT = CONFIG.timeout
String TIMER = CONFIG.timer
def UPSTREAM = CONFIG.upstream
if (UPSTREAM instanceof String) {
  UPSTREAM = [
    upstreamProjects: CONFIG.upstream,
    threshold: "SUCCESS",
  ]
}
def ANSIBLE_EXTRA_VARS_LOCAL
String ENVIRONMENT
String TARGET
String BRANCH

def ENVIRONMENTS = VARS.environments.findAll{it.value != false}

if ( !params.RECONFIGURE_PIPELINE ) {
  /* Runtime vars */
  if (env.gitlabBranch) {
    BRANCH = env.gitlabBranch
    ENVIRONMENT = ENVIRONMENTS.findAll{it.value.branch == BRANCH && it.value.trigger != false}.collect{it.key}.first()
  }
  else {
    ENVIRONMENT = params.environment
    BRANCH = ENVIRONMENTS[ENVIRONMENT]['branch']
  }
  TARGET = ENVIRONMENTS[ENVIRONMENT]['target']
  TARGET = CONFIG.inventory_maps[TARGET] ?: TARGET

  /* Populate job vars */
  LOCK = "${CONFIG.name}-${ENVIRONMENT}"
  ANSIBLE_EXTRA_VARS_LOCAL=[
    appenv: ENVIRONMENT,
    target: TARGET,
  ]
}

pipeline {
  agent any
  options {
    skipDefaultCheckout()
    ansiColor('xterm')
    lock(LOCK)
    timeout(time: TIMEOUT, unit: 'MINUTES')
    gitLabConnection(CONFIG.gitlab_id)
  }
  triggers {
    cron(TIMER)
    upstream(
      upstreamProjects: UPSTREAM.upstreamProjects,
      threshold: UPSTREAM.threshold,
    )
    gitlab(
     triggerOnPush: true,
     //triggerOnMergeRequest: true,
     setBuildDescription: true,
     branchFilterType: 'NameBasedFilter',
     includeBranchesSpec: "${ENVIRONMENTS.findAll{it.value.trigger != false}.collect{it.value.branch}.join(',')}",
     excludeBranchesSpec: '',
    )
  }
  stages {
    stage('init') {
      steps {
        script {
          currentBuild.description = "Run QA ${BRANCH} on ${ENVIRONMENT} (${TARGET}): ${currentBuild.description ?: '-'}"
        }
      }
    }
    stage('git') {
      steps {
        checkout([
          $class: 'GitSCM',
          branches: [[name: BRANCH]],
          userRemoteConfigs: [[
            credentialsId: VARS.repo.credentials_id,
            url: VARS.repo.url,
          ]],
          doGenerateSubmoduleConfigurations: false,
          extensions: [
            [
              $class: 'CleanCheckout'
            ],
            [
              $class: 'PruneStaleBranch'
            ],
            [
              $class: 'SubmoduleOption',
              disableSubmodules: false,
              parentCredentials: true,
              recursiveSubmodules: true,
              reference: '',
              trackingSubmodules: false
            ]
          ],
          submoduleCfg: [],
        ])
      }
    }
    stage('build') {
      steps {
        // Credentials may be needed by composer.
        runAnsiblePlaybook(CONFIG.ansible, VARS.build.playbook, ANSIBLE_EXTRA_VARS_LOCAL, VARS.repo.credentials_id)
      }
      when {
        expression {
          return (boolean)CONFIG.build_before && (boolean)VARS.build.playbook
        }
      }
    }
    stage('qa') {
      steps {
        runQATools(VARS.qa)
      }
      post {
        always {
          publishQAResults(VARS.qa)
        }
      }
    }
  }
  post {
    cleanup {
      script {
        if (params.RECONFIGURE_PIPELINE || CONFIG.cleanWS) {
          cleanWs()
        }
      }
    }
  }
}

// vi:syntax=groovy
