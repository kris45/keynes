package io.sbit.jenkins.keynes

import groovy.io.FileType
import java.util.regex.Matcher
import org.jenkinsci.plugins.pipeline.modeldefinition.ast.ModelASTPipelineDef
import org.jenkinsci.plugins.pipeline.modeldefinition.parser.Converter

/**
 * Generates definitions of jobs, folders, etc to be instantiated by jobDsl.
 *
 * This is also a tricky singleton class used to store Global variables.
 * We can't take advantage of @Singleton annotation because of
 * the need for @NonCPS.
 */
class Seed {
  private static final Seed instance = new Seed();

  String workspace
  String baseFolder
  String development
  def steps

  private Seed() {
  }

  @NonCPS
  public static Seed getInstance() {
    return instance;
  }

  public void setGlobals(String workspace, String baseFolder, Boolean development, def pipelineSteps) {
    this.workspace = workspace
    this.baseFolder = baseFolder
    this.development = development
    this.steps = pipelineSteps
  }


  private def configFiles = [:]
  private def folders = [:]
  private def jobs = [:]

  /**
   * Grow the seed.
   */
  public void process() {
    // Add base folder containing all jobs.
    if (baseFolder) {
      addFolder(baseFolder)
    }

    // Process projects and add all jobs.
    processProjects("$workspace/projects")
  }

  /**
   * Process all projects in the given directory and generate folders, config files and jobs.
   */
  def processProjects(String dir) {
    ConfigObject baseConfig = steps.readYamlConfig("$dir/config.yml")
    addConfigFiles(baseConfig.files)
    baseConfig.remove('files')

    def projects = steps.directoryListing(dir, FileType.DIRECTORIES)
    projects.each { name, path ->
      Utils.log "Processing project ${name}"
      Project project = new Project(name, path, baseConfig)

      // Add project base folder.
      addFolder(project.folder, project.label, project.description, project.authorization)

      // Add project specific config files.
      addConfigFiles(project.config.files, [path])

      // Add a job for each pipeline variant.
      String jobFolder
      project.pipelines.each { pipelineName, pipeline ->
        if (pipeline.config.jobFolder) {
          jobFolder = "${project.folder}/${pipeline.name}"
          addFolder(jobFolder, pipeline.label, pipeline.description)
        }
        else {
          jobFolder = project.folder
        }

        pipeline.variants.each { variantName, variant ->
          addJob("${jobFolder}/${variant.name}", variant.label, variant.description, variant.authorization, variant.script, variant.params, variant.config)
        }
      }
    }
  }

  /**
   * Add Jenkins job.
   */
  private void addJob(String name, String label, String description, Map authorization, String script, Map params, ConfigObject config) {
    script = addParameters(script, params)
    script = addPipelineConfigStep(script)
    if (validateScript(script, name)) {
      this.jobs[name] = [
        'name': name,
        'label': label,
        'description': description,
        'authorization': authorization,
        'script': script,
        'config': config.toProperties(),
      ]
    }
  }

  /**
   * Add Jenkins folder.
   */
  private void addFolder(String name, String label = '', String description = '', Map authorization = [:]) {
    this.folders[name] = [
      'name': name,
      'displayName': label ?: name,
      'description': description,
      'authorization': authorization,
    ]
  }

  /**
   * Add Jenkins config files.
   */
  private void addConfigFiles(files, paths = []) {
    files.each { filename, file ->
      String fileId = filename.replace('/', '__')
      // base files can't be redefined in a project.
      if (this.configFiles.containsKey(fileId)) {
        Utils.warn "Redefining files is not allowed. Ignoring ${fileId}."
        return
      }

      String content = file.get('content', false) ?: steps.loadResource(file.get('path', filename), paths, true)
      this.configFiles[fileId] = [
        'id'      : fileId,
        'name'    : file.get('name', filename),
        'comment' : file.get('comment', filename),
        'content' : content,
      ]
    }
  }

  /**
   * Validate a Declarative Pipeline script.
   */
  private def validateScript(String script, String name) {
    try {
      ModelASTPipelineDef pipelineDef = Converter.scriptToPipelineDef(script)
      if (pipelineDef != null) {
        return true
      } else {
        Utils.err "Declarative Pipeline script for ${name} does not contain the 'pipeline' step."
        return false
      }
    } catch (Exception e) {
      Utils.err "Error(s) validating Declarative Pipeline script for ${name} - " + e.toString()
      return false
    }
  }

  /**
   * Inject parameters in the pipeline script.
   */
  private def addParameters(String script, Map params) {
    String code = '''
      parameters {
        booleanParam(name: 'RECONFIGURE_PIPELINE', defaultValue: false, description: 'This option is used to reconfigure the pipeline. It is expected to produce a failed build.')
    '''.stripIndent(4)

    params.each { k, v ->
      // Parameter containing a 'value' key are considered read only.
      // Read only is implemented with a one-option choice widget.
      // @see Pipeline::generateVariant()
      if (v.readonly) {
        if (v.type != 'boolean') {
          v.value = "'${v.value}'"
        }
        v.type = 'readonly'
        v.description += " (hardcoded)"
      }

      String param = "name: \"${k}\", description: \"${v.description}\""
      switch (v.type) {
        case 'boolean':
          param = "booleanParam(" + param + ", defaultValue: ${v.defaultValue})"
          break;
        case 'choice':
          param = "choice(" + param + ", choices: \"${v.choices}\")"
          break;
        case 'string':
          param = "string(" + param + ", defaultValue: \"${v.defaultValue}\")"
          break;
        case 'readonly':
          param = "choice(" + param + ", choices: [${v.value}])"
          break;
      }
      code += '    ' + param + "\n"
    }

    code = Matcher.quoteReplacement(code)
    if ( script =~ /parameters\s*\{/ ) {
      script = script.replaceFirst(/parameters\s*\{/, code)
    }
    else {
      script = script.replaceFirst(/stages\s*\{/, "${code}  }  stages {")
    }

    return script
  }

  /**
   * Adds an extra parameter and step to a pipeline script in order to
   * control the first execution after deploying a new version of the seed.
   */
  private String addPipelineConfigStep(String script) {
    String stage = '''
      stages {
        stage('pipeline-config') {
          steps {
            script {
              if ( params.RECONFIGURE_PIPELINE ) {
                echo "Aborted after reconfiguring the pipeline."
                currentBuild.description = "Reconfigure pipeline (expected fail)"
                currentBuild.result = 'ABORTED'
                error(currentBuild.description)
              }
            }
          }
        }'''.stripIndent(4)

    stage = Matcher.quoteReplacement(stage)
    script = script.replaceFirst(/stages\s*\{/, stage)

    return script
  }

}
