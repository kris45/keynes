package io.sbit.jenkins.keynes

class Pipeline {
  String name
  String label
  String description
  String script
  ConfigObject config
  ConfigObject vars
  Map params = [:]
  def variants
  private def steps

  Pipeline(String name, ConfigObject settings, String script, Project project) {
    this.name = name
    this.label = settings.name ?: name
    this.description = settings.description ?: "${project.label}: ${this.label}"
    this.script = script
    this.steps = Seed.getInstance().steps

    // Config.
    this.config = mergeConfig(project.config, (ConfigObject)settings.config)

    // Vars.
    ConfigObject pipelineVars = new ConfigObject()
    ArrayList yaml = steps.loadResource("pipelines/${settings.basename}.yml", [project.path], false, true)
    yaml.reverse().each {
      pipelineVars.merge(steps.readYamlConfig(null, it))
    }
    this.vars = mergeVars(pipelineVars, (ConfigObject)settings.vars)

    // Extract params from vars.
    if (vars.containsKey('params')) {
      vars.params.each { k, v ->
        // Allow default params to be unset.
        if (v == false) return
        Map defaults = ['description': '', 'readonly': false]
        params.put(k, defaults + v)
      }
      vars.remove('params')
    }

    // Variants.
    if (settings.variants) {
      this.variants = processVariants(settings.variants)
    }
    else {
      // If there're no variants generate a default one.
      this.config.jobFolder = false
      Map item = generateVariant(this.name, this.label, this.description)
      this.variants = ["${this.name}": item]
    }
  }

  /**
   * Returns a ConfigObject with pipeline variant default configuration.
   */
  @NonCPS
  private ConfigObject defaultConfig() {
    new ConfigObject([
      'name'      : this.name,
      'enabled'   : true,
      'jobFolder' : true,
      'timer'     : '',
      'upstream'  : '',
      'cleanWS'   : true,
    ])
  }

  /**
   * Merge default, project and pipeline config.
   */
  @NonCPS
  private ConfigObject mergeConfig(ConfigObject projectConfig, ConfigObject pipelineConfig) {
    ConfigObject config = defaultConfig()

    // Project config. Exclude 'pipelines' and all entries starting with '_'.
    ArrayList excludes = ['files', 'pipelines']
    config.merge(Utils.deepCopy((ConfigObject)(projectConfig.findAll{!excludes.contains(it.key) && !it.key.startsWith('_')})))

    config.merge(pipelineConfig)

    return config
  }

  /**
   * Merge default, pipeline's .yml and pipeline instance vars.
   */
  @NonCPS
  private ConfigObject mergeVars(ConfigObject pipelineVars, ConfigObject pipelineInstanceVars) {
    ConfigObject vars = new ConfigObject()
    vars.merge(pipelineVars)
    vars.merge(pipelineInstanceVars)

    return vars
  }

  /**
   * Generate variants of the pipeline.
   */
  @NonCPS
  private def processVariants(Map variants) {
    def items = [:]

    variants.each { name, settings ->
      // Allow inherited variant to be skipped by overwritting them to false.
      if (settings == false) return

      String label = settings.name ?: name
      String description = settings.description ?: "${this.description} - ${label}"

      Map item = generateVariant(name, label, description, (ConfigObject)settings)
      items[name] = item
    }

    return items
  }

  /**
   * Generate a variant.
   */
  @NonCPS
  private def generateVariant(String name, String label, String description, ConfigObject settings = [:]) {
    ConfigObject config = Utils.deepCopy(this.config)
    config.merge((ConfigObject)settings.config)
    if (Seed.getInstance().development) {
      config.timer = ''
    }
    Map authorization = (HashMap)config.authorization
    config.remove('authorization')
    String config_json = new groovy.json.JsonBuilder(config).toPrettyString()

    ConfigObject vars = Utils.deepCopy(this.vars)
    vars.merge((ConfigObject)settings.vars)
    String vars_json = new groovy.json.JsonBuilder(vars).toPrettyString()

    Map params = [:]
    this.params.each {k, v ->
      if (vars.containsKey(k)) {
        v = v + ['readonly': true, 'value': vars[k]]
      }
      params.put(k, v)
    }

    String script = script.replace("#!groovy", "#!groovy" + """

def CONFIG_json = \"\"\"${config_json}\"\"\"
def VARS_json = \"\"\"${vars_json}\"\"\"
def CONFIG = readJSON text: CONFIG_json
def VARS = readJSON text: VARS_json""")

    return [
      'name': name,
      'label': label,
      'description': description,
      'authorization': authorization,
      'script': script,
      'params': params,
      'config': config,
    ]
  }

}
