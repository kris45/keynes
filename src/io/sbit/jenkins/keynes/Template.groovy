package io.sbit.jenkins.keynes

class Template {

  Map config
  int failures = 0

  Template(Map config) {
    this.config = config

    while ({
      int failures = this.failures
      this.failures = 0
      try {
        traverse(config)
      }
      catch(Exception e) {
        throw new Exception("Failed traversing config:\n\n${config}\n\n. Exception:\n${e}\n")
      }
      (this.failures > 0 && this.failures != failures)
    }()) continue
  }

  @NonCPS
  private static boolean isTemplate(def entry) {
    return ((entry instanceof String) && (entry.startsWith('#!')))
  }

  /**
   * Process a template entry.
   *
   * Original entry is returned on failure.
   */
  @NonCPS
  private def process(String entry) {
    def result = null
    String sheebang = entry.substring(0, entry.indexOf(' '))
    String expression = entry.substring(sheebang.length() + 1)
    String lang = sheebang.substring(2)
    switch (lang) {
      case 'groovy':
        try {
          result = Eval.me('config', this.config, expression)
        }
        catch(Exception e) {
          Utils.err "Failed to interpolate ${entry}': ${e}"
        }
        break
      default:
        Utils.err "Unsupported templating language: ${lang}"
    }

    // If there's no result because of an exception or unsupported lang,
    // signal a failure by returning the original entry.
    // OTOH, discard results containing a sheebang, those are probably early
    // interpolations that should be interpolated later when the target
    // entry is already interpolated.
    if (!result || ((result instanceof String) && (result.startsWith('#!')))) {
      result = entry
    }
    result
  }

  @NonCPS
  public traverse(data) {
    if (data instanceof Map) {
      data.each { entrySet ->
        if (isTemplate(entrySet.value)) {
          def result = process(entrySet.value)
          (result == entrySet.value) ? this.failures++ : (entrySet.value = result)
        }
        else {
          entrySet.value = traverse(entrySet.value)
        }
      }
    }
    else if (data instanceof Collection) {
      def processedList = []
      data.each { entry ->
        if (isTemplate(entry)) {
          def result = process(entry)
          (result == entry) ? this.failures++ : (processedList << result)
        }
        else {
          processedList << traverse(entry)
        }
      }
      data = processedList
    }
    data
  }
}
