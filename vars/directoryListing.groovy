#!groovy

import groovy.io.FileType

/**
 * Returns a map of files in a directory.
 *
 * key: filename
 * value: the abs path
 */
@NonCPS
def call(String path, FileType fileType = FileType.ANY) {
  def files = [:]

  File dir = new File(path)
  if (dir.exists()) {
    dir.eachFile (fileType) { file ->
      String name = file.getName()
      files << ["${name}": file.getPath()]
    }
  }

  return files
}

