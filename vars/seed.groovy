#!groovy

import io.sbit.jenkins.keynes.Seed

def call(String baseFolder = '', development = null) {
  // If no base folder specified and the job name ends with [-_]dev|devel,
  // set the base folder to parent/dev|devel.
  // Example: for admin/seed-dev job, set admin/dev base folder.
  if (!baseFolder) {
    def matcher = (env.JOB_BASE_NAME =~ /^.*[-_](dev|devel)$/)
    if (matcher) {
      parentFolder = (env.JOB_NAME == env.JOB_BASE_NAME) ? '' : env.JOB_NAME.substring(0, env.JOB_NAME.lastIndexOf('/'))
      baseFolder = "${parentFolder}/${matcher[0][1]}"
      if (development == null) {
        development = true
      }
    }
  }

  Seed seed = Seed.getInstance()
  seed.setGlobals(env.workspace, baseFolder, development, this)
  seed.process()

  jobDsl(
    failOnMissingPlugin: true,
    ignoreMissingFiles: true,
    removedConfigFilesAction: 'DELETE',
    removedJobAction: 'DELETE',
    removedViewAction: 'DELETE',
    unstableOnDeprecation: true,
    additionalParameters: [
      seedConfigFiles: seed.configFiles,
      seedFolders: seed.folders,
      seedJobs: seed.jobs,
    ],
    scriptText: libraryResource('jobDsl/seed.groovy'),
    sandbox: true,
  )
}

