# Keynes

Jenkins Pipeline Shared Library to feed JobDSL.

See [Keynes-example](https://gitlab.com/sbitio/keynes-example) repository for a
complete reference on features, setup and usage.

## Requirements

Minimum requirements are:

* [Git](https://plugins.jenkins.io/git)
* [Job DSL](https://plugins.jenkins.io/job-dsl)
* [Pipeline](https://plugins.jenkins.io/workflow-aggregator)
* [Pipeline Utility Steps](https://plugins.jenkins.io/pipeline-utility-steps)
* [Folders](https://plugins.jenkins.io/cloudbees-folder)
* [Config File Provider](https://plugins.jenkins.io/config-file-provider) (Optional)

Additionally, the provided pipelines require at least:

* [Ansible](https://plugins.jenkins.io/ansible)
* [AnsiColor](https://plugins.jenkins.io/ansicolor)
* [Lockable Resources](https://plugins.jenkins.io/lockable-resources)
* [SSH Agent](https://plugins.jenkins.io/ssh-agent)
* [Workspace Cleanup](https://plugins.jenkins.io/ws-cleanup)

For full features, the provided pipelines require:
* [Docker Pipeline](https://plugins.jenkins.io/docker-workflow/)
* [Gatling](https://plugins.jenkins.io/gatling/)
* [HTML Publisher](https://plugins.jenkins.io/htmlpublisher/)
* [Violation Comments to GitLab](https://plugins.jenkins.io/violation-comments-to-gitlab/)
* [Warnings Next Generation](https://plugins.jenkins.io/warnings-ng/)

## Development

Development happens on [Gitlab](https://gitlab.com/sbitio/keynes).

Please log issues for any bug report, feature or support request.

## License

MIT License, see LICENSE file

## Contact

Please use the contact form on http://sbit.io
